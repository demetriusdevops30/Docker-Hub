#!/usr/bin/env python
# coding: utf-8

# In[2]:


"""Implements a discrete version of batch constrained q learning"""
import argparse
import os 
import pickle
import copy
import torch
import numpy as np

from model.config import get_config_from_dir
from model.utils import Vocab, convert_old_checkpoint_formar, detokenize, to_var
from model.solver import Solver, VariationalSolver
from model.data_loader import get_loader
from model.models import VariationalModels


def load_pickle(path):
    with open(path, 'rb') as f:
        return  pickle.load(f)
    
    
def parse_config_args():
    parser = argparse.ArgumentParser()
    
    #must provide checkpoints of pre-trained models to load
    parser.add_argument('--checkpoint', type = str, default = None)
    parser.add_argument('--q_checkpoint', type = str, default = None)
    
    parser.add_argument('--bcq_n', type = int, default = 10,
                        help = "Number of samples from prior in BCQ algorithm.")
    
    return vars(parser.parse_args())


class DBCQ:
    
    def _init_(self, prior_config, rl_config, beam_size = 5):
        self .prior_config = prior_config
        self.rl_config = rl_config
        self.rl_config.beam_size = beam_size
        
        print('Loading Vocabulary...')
        self.vocab = Vocab()
        self.vocab.load(prior_config.word2id_path, prior_config.id2word_path)
        self.prior_config.vocab_size = self.vocab.vocab_size
        self.rl_config.vocab_size = self.vocab.vocab_size
        print(f'Vocabulary size: {self.vocab.vocab_size}')
        
        self.eval_data = self.get_data_loader()
        self.build_models()
        
    def build_models(self):
        rl_config = copy.deepcopy(self.rl_config)
        rl_config.checkpoint = None
        
        print("Building Q Network")
        if rl_config.model in VariotionalModels:
            self.q_net = VariationalModels:
                self.q_net = variationalSolver(
                        rl_config, None, self.eval_data, vocab = self.vocab,
                        is_train = false)
        else:
            self.q_net = Solver(
                    rl_config, None, self.eval_data, vocab = self.vocab,
                    is_train = false)
            self.q_net.build()
            self.load_q_network()
            
            print('Building prior network')
            if self.prior_config.model in VariationalModels:
                sel.pretained_prior = VariationalSolver(
                        self.prior_config, None, self.eval_data, vocab = self.vocab,
                        is_train = False)
                self.pretained_prior.build()
                
                #freeze the weights so they stay constant
                self.pretained_prior.model.eval()
                for params in self.pretained_prior.model.parameters():
                    params.required_grad = False
                    
    def load_q_network(self):
        """load parameters from rl checkpoint"""
        print(f"Carregando parametros de Q na rede {self.rl_config.checkpoint}")
        q_ckpt = torch.load,(self.rl_config.checkpoint)
        q_ckpt = convert_old_checkpoint_format(q_ckpt)
        self.q_net.model.load_state_dict(q_ckpt)
        
        #ensure weigths are initialized to be on the gpu when necessary
        if torch.cuda.is_available():
            print('Convertendo modelo de checkpoint do tensor cuda')
            self.q_net.model.cuda()
        
        def get_data_loader(self):
        # If checkpoint is for an emotion model, load that pickle file
        emotion_sentences = None
        if self.prior_config.emotion:
            emotion_sentences = load_pickle(self.prior_config.emojis_path)

        # Load infersent embeddings if necessary
        infersent_sentences = None
        if self.prior_config.infersent:
            print('Loading infersent sentence embeddings...')
            infersent_sentences = load_pickle(self.prior_config.infersent_path)
            embedding_size = infersent_sentences[0][0].shape[0]
            self.prior_config.infersent_output_size = embedding_size

        return get_loader(
            sentences=load_pickle(self.prior_config.sentences_path),
            conversation_length=load_pickle(
                self.prior_config.conversation_length_path),
            sentence_length=load_pickle(self.prior_config.sentence_length_path),
            vocab=self.vocab,
            batch_size=self.prior_config.batch_size,
            emojis=emotion_sentences,
            infersent=infersent_sentences)

    def interact(self, max_conversation_length=5, sample_by='priority', 
                 debug=True):
        model_name = self.prior_config.model
        context_sentences = []

        print("Time to start a conversation with the chatbot! It's name is", 
              model_name)
        username = input("What is your name? ")

        print("Let's start chatting. You can type 'quit' at any time to quit.")
        utterance = input("Input: ")
        print("\033[1A\033[K")  # Erases last line of output

        while (utterance.lower() != 'quit' and utterance.lower() != 'exit'):
            # Process utterance
            sentences = utterance.split('/')

            # Code and decode user input to show how it is transformed for model
            coded, lens = self.pretrained_prior.process_user_input(sentences)
            decoded = [self.vocab.decode(sent) for i, sent in enumerate(
                coded) if i < lens[i]]
            print(username + ':', '. '.join(decoded))

            # Append to conversation
            context_sentences.extend(sentences)

            gen_response = self.generate_response_to_input(
                context_sentences, max_conversation_length, sample_by=sample_by,
                debug=debug)

            # Append generated sentences to conversation
            context_sentences.append(gen_response)

            # Print and get next user input
            print("\n" + model_name + ": " + gen_response)
            utterance = input("Input: ")
            print("\033[1A\033[K")

    def process_raw_text_into_input(self, raw_text_sentences,
                                    max_conversation_length=5, debug=False,):
        sentences, lens = self.pretrained_prior.process_user_input(
            raw_text_sentences, self.rl_config.max_sentence_length)

        # Remove any sentences of length 0
        sentences = [sent for i, sent in enumerate(sentences) if lens[i] > 0]
        good_raw_sentences = [sent for i, sent in enumerate(
            raw_text_sentences) if lens[i] > 0]
        lens = [l for l in lens if l > 0]

        # Trim conversation to max length
        sentences = sentences[-max_conversation_length:]
        lens = lens[-max_conversation_length:]
        good_raw_sentences = good_raw_sentences[-max_conversation_length:]
        convo_length = len(sentences)

        # Convert to torch variables
        input_sentences = to_var(torch.LongTensor(sentences))
        input_sentence_length = to_var(torch.LongTensor(lens))
        input_conversation_length = to_var(torch.LongTensor([convo_length]))

        if debug:
            print('\n**Conversation history:**')
            for sent in sentences:
                print(self.vocab.decode(list(sent)))

        return (input_sentences, input_sentence_length, 
                input_conversation_length)

    def duplicate_context_for_beams(self, sentences, sent_lens, conv_lens,
                                    beams):
        conv_lens = conv_lens.repeat(len(beams))
        # [beam_size * sentences, sentence_len]
        if len(sentences) > 1:
            targets = torch.cat(
                [torch.cat([sentences[1:,:], beams[i,:].unsqueeze(0)], 0) 
                for i in range(len(beams))], 0)
        else:
            targets = beams

        # HRED
        if self.rl_config.model not in VariationalModels:
            sent_lens = sent_lens.repeat(len(beams))
            return sentences, sent_lens, conv_lens, targets
        
        # VHRED, VHCR
        new_sentences = torch.cat(
            [torch.cat([sentences, beams[i,:].unsqueeze(0)], 0) 
            for i in range(len(beams))], 0)
        new_len = to_var(torch.LongTensor([self.rl_config.max_sentence_length]))
        sent_lens = torch.cat(
            [torch.cat([sent_lens, new_len], 0) for i in range(len(beams))])
        return new_sentences, sent_lens, conv_lens, targets

    def generate_response_to_input(self, raw_text_sentences, 
                                   max_conversation_length=5,
                                   sample_by='Priority', emojize=True,
                                   debug=True):
        with torch.no_grad():
            (input_sentences, input_sent_lens, 
             input_conv_lens) = self.process_raw_text_into_input(
                raw_text_sentences, debug=debug,
                max_conversation_length=max_conversation_length)

            # Initialize a tensor for beams
            beams = to_var(torch.LongTensor(
                np.ones((self.rl_config.beam_size, 
                        self.rl_config.max_sentence_length))))

            # Create a batch with the context duplicated for each beam
            (sentences, sent_lens, 
            conv_lens, targets) = self.duplicate_context_for_beams(
                input_sentences, input_sent_lens, input_conv_lens, beams)

            # Continuously feed beam sentences into networks to sample the next 
            # best word, add that to the beam, and continue
            for i in range(self.rl_config.max_sentence_length):
                # Run both models to obtain logits
                prior_output = self.pretrained_prior.model(
                    sentences, sent_lens, conv_lens, targets, rl_mode=True)
                all_prior_logits = prior_output[0]
                
                q_output = self.q_net.model(
                    sentences, sent_lens, conv_lens, targets, rl_mode=True)
                all_q_logits = q_output[0]

                # Select only those logits for next word
                q_logits = all_q_logits[:, i, :].squeeze()
                prior_logits = all_prior_logits[:, i, :].squeeze()

                # Get prior distribution for next word in each beam
                prior_dists = torch.nn.functional.softmax(prior_logits, 1)

                for b in range(self.rl_config.beam_size):
                    # Sample from the prior bcq_n times for each beam
                    dist = torch.distributions.Categorical(prior_dists[b,:])
                    sampled_idxs = dist.sample_n(self.rl_config.bcq_n)

                    # Select sample with highest q value
                    q_vals = torch.stack(
                        [q_logits[b, idx] for idx in sampled_idxs])
                    _, best_word_i = torch.max(q_vals, 0) 
                    best_word = sampled_idxs[best_word_i]

                    # Update beams
                    beams[b, i] = best_word

                (sentences, sent_lens, 
                 conv_lens, targets) = self.duplicate_context_for_beams(
                    input_sentences, input_sent_lens, input_conv_lens, beams)
            
            generated_sentences = beams.cpu().numpy()

        if debug:
            print('\n**All generated responses:**')
            for gen in generated_sentences:
                print(detokenize(self.vocab.decode(list(gen))))
        
        gen_response = self.pretrained_prior.select_best_generated_response(
            generated_sentences, sample_by, beam_size=self.rl_config.beam_size)

        decoded_response = self.vocab.decode(list(gen_response))
        decoded_response = detokenize(decoded_response)

        if emojize:
            inferred_emojis = self.pretrained_prior.botmoji.emojize_text(
                raw_text_sentences[-1], 5, 0.07)
            decoded_response = inferred_emojis + " " + decoded_response
        
        return decoded_response


if __name__ == '__main__':

    kwargs_dict = parse_config_args()

    kwargs_dict['mode'] = 'valid'
    prior_config = get_config_from_dir(kwargs_dict['checkpoint'], **kwargs_dict)

    kwargs_dict['load_rl_ckpt'] = True
    rl_config = get_config_from_dir(kwargs_dict['q_checkpoint'], **kwargs_dict)

    dbcq = DBCQ(prior_config, rl_config)
    dbcq.interact()


# In[ ]:


from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps

db_connect = create engine('sqlite:///simmclient.mongodb')
app = Flask(_name_)
api = Api(app)
outlier = api


# In[ ]:


#criando netscan do banco de dados
de_future_import print function
#ipmport datetime #time stamp

import pcapy #mapeamento passivo
import os #check sudo
import pacotes de analise dpkt
import binascii #get mac addr on messages ARP# 
#import netaddr #endereços ipv4 / 6, espaço de endereço: 192.168.5.0/24
import impressão como pp # informações de exibição
#comandos de importação #arp-scan
#solicitações de importação #mac api
soquete de importação #pedido
import sys #obter plataforma (linux ou linux2)
#subprocesso de importação #use commandline
#importrandom #pinger o usa ao criar pacotes ICMP
#da lib import WhoIs
#do wol de importação acordado #wake on lan

""" Executando comando no terminal do servidor 
sudo tcpdump -s 0 -i en1 -w test.pcap
-s 0 definirá o byte de captura para seu máximo ou seja 65535 e não truncará
-i en1 capturará a interface Ethernet
-w test.cap criará esse arquivo pcap
tcpdump -qns 0 -X -r osx.pcap
$sudo tcpdump -w osx.pcap
tcpdump: tipo de link de dados PKTAP
tcpdump: escutando no pktap, PKTAP do tipo link (Tapet Tap), tamanho da captura 65535 bytes
^ Pacotes C4414 capturados 
4416 pacotes recebidos pelo filtro
0 pacotes descartados pelo kernel"""


"""
####################### Saida dos dados no terminal 
# class DNS (objeto):
# def __init (self, udp) __:
# dns = dpkt.dns.DNS (udp.data)
# para rr em dns.an:
# h = self.getRecord (rr)
# print h
"""
classe ARP(objeto):
    def _init_(self, arp):
        user.msg = {}
        se arp.op == dpkt.arp. ARP_OP_REPLY:
            user.msg: {
                    'type': 'arp', 
                    'mac': self.add_colons_to_mac(bininas.heslify(arp, sha))
                    'ipv4': soquete.inet_ntoa(arp.spa)
                    }
            return
        def get(próprio):
            return user.msg
        
        def add_colons_to_mac(self, mac_addr):
            """ Execução automatica após teste de terminal(sequencia de fibonacci)
            Está função aceita uma sequencia de 12 digitos hexadecimais e a converte em dois pontos
            sequencia separada
            """
            
            s = lista()
            for i in range(12 / 2): #mac_addr deve ser sempre 12 caracteres, trabalhamos em um grupo de 2 digitos
                s.acrescentar(mac_addr [i * 2 : I * 2 + 2])]
    r = ":" . junção(s)
    return r

class mDNS(objeto):
    def _init_(self, udp):
        user.msg = {}
        tente: 
            mdns = dpkt.DNS.DNS(dadosdup.)
        except dpkt.error:
            #print "mDNS dpkt.Error'
            return
        except(IndexError, TypeError):
            #dpkt não deve fazer isso, mas em alguns casos
            #print "mDNS outro erro"
            return
        except:
            #print "mDNS crap:", sys.exc_info()
            #print "mDNS outro erro"
            return
        except:
             #print "mDNS crap:", sys.exc_info()
             #print udp
             return
         if mdns.qr != dpkt.DNS, DNS_R: return
         if mdns.qr.opcode != dpkt.DNS, DNS_QUERY: return
         if mdns.qr.rcode != dpkt.DNS, DNS_RCODENOERR: return
         
         user.msg[type] = 'mdns'
         ans =[]
         
         para rr em mdns.um:
             h = auto.getRecord(rr)
             #verifique se está vazio
             se h:ans.insert(h)
        user.msg['rr'] = ans
        return


# In[ ]:


#executando segund parte do net scan

def getRecord(self, rr):
    """ os registros de resposta (rr) em um pacote DNS referem-se ao mesmo host(host local)
    """
    
		se  rr . type  ==  1 : retorna { 'type' : 'a' , 'ipv4' : socket . inet_ntoa ( rr . rdata ), 'hostname' : rr . nome }
		elif  rr . type  ==  28 : retornar { 'type' : 'aaaa' , 'ipv6' : socket . inet_ntop ( socket . AF_INET6 , rr . rdata ), 'hostname' : rr . nome }
		elif  rr . type  ==  5 : retornar { 'type' : 'cname' , 'hostname' : rr . nome , 'cname' : rr . cname }
		elif  rr . type  ==  13 : retornar { 'type' : 'hostinfo' , 'hostname' : rr . nome , 'info' : rr . rdata }
		elif  rr . type  ==  33 : retornar { 'type' : 'srv' , 'hostname' : rr . srvname , 'porta' : rr . porta , 'srv' : rr . nome . split ( '.' ) [ - 3 ], 'proto' : rr . nome . divisão ( '.' ) [ - 2 ]}
		elif  rr . type  ==  12 : retornar { 'type' : 'ptr' }
		elif  rr . type  ==  16 : retornar { 'type' : 'txt' }
		elif  rr . type  ==  10 : return { 'type' : 'wtf' }
def get(proprio):
    retornar a usuario.msg
    
class PacketDecoder(object):
    """ Encoder primário a ser instalado localmente pela equipe devops
    PacketDecoder le os pacotes dpkt e produz um ditado com informações uteis na rede
    recon. Nem tudo é usado atualmente.
    eth: hw addr src, dst
    -tcp: porta src,dst; sequencia num;
    -udp: porta src, dst;
    -dns: opcode; rcode;
    -RR:
        -- TXT: ?
        a: ipv4; nome do anfitrião
        aaaa: ipv6, nome do anfitrião
        ptr:?
        cname:?
        srv: nome do host, serviço; protocolo; porta
    -Q:
ipv6: endereço IP src, dst; nxt
-icmpv6:
"""
ipMap = {}
def getip(self, ip, ipv6 = false):
    if ipv6:
        tomada de return.inet_ntop(socket.AF_INET6, ip)
    else:
        tomada de return.inet_ntoa(ip)
decodificação def(self, eth):
    """ Decodificar um pacote ethernet.O dict retornado o tipo(arp, mdns, etc)
    que indicara como ler / usar o ditado.
    	https://support.apple.com/en-us/HT202944
        
        em : ethernet pkt
        out: dict
        execução do encoder com codigo fonte instalado localmente"""
if eth.digite == dpkt.ethernet.ETH_TYPE_ARP:
    "print 'ARP'"
    return ARP (data de et.).get()
elif eth.digite == dpkt.ethernet. ETH_TYPE_IP06:
    ip = eth.data
    
    #multicast é com o IPV4
    if udp.dport == 5353:
        #print udp
        ans = mDNS(udp).get()
        #print 25 * '='
        #pp.print (ans)
        #print 25 * '='
        return ans
   
    # print 'IPv6 UDP', 'porta:', udp.dport, 'src:', self.getip (ip.src, True), 'dst:', self.getip (ip.dst, True)

#tcp não é util 
    elif ip.p == dpkt.ip. IP_PROTO_TCP:
        pass
    tcp = ip.data
    #print 'IPv6 TCP', 'port:', tcp.dport, 'src:', self.getip(ip.src, true), 'dst:', self.getip(ip.dst, true)
    #msg de erro ICMP não é util para mapeamento
elif ip.p == dpkt.ip. IP_PROTO_ICMP6:
    # PRINT 'IPv6, icmp6:' ip.data.data
    pass
sum:
    pass
# print 'IPv6', ip.p, 'src:', self.getip (ip.src, True), 'dst:', self.getip (ip.dst, True)
elif eth.input == dpkt.ethernet.ETH_TYPE_IP:
    ip = eth.data
    #porta da interface Roku: 1900 dst: 239.255.255.250 1900
    if ip.p == dpkt.ip.IP_PROTO_UDP:
        udp = ip.data
    elif:
        udp.port == 53:
            return DNS (udp.data)
        return {}
    elif:
        udp.port == 5353:
            'print mDNS'
            'print udp'
             return mDNS(udp).get()
         elif.auto.getip(ip.dst) == '239.255.255.250':
             return {}
         else:
             
					# não imprime portas padrão
					# 17500 dropbox
					# se não for ip.data.dport em [17500]:
					# print 'other udp', 'port:', udp.dport, 'src:', self.getip (ip.src), 'dst:', self.getip (ip.dst), ':'
                    return {}
        elif:
            ip.p == dpkt.ip.IP_PROTO_TCP:
                # src = self.getip (ip.src)
				# if netaddr.IPAddress (src) não está em netaddr.IPNetwork ("192.168.1.0/24"):
				# who = ''
				# if src não está em self.ipMap:
				# who = WhoIs (src) .record ['NetName']
				# self.ipMap [src] = quem
				# outro:
				# who = self.ipMap [src]
				# if who in ['GOOGLE' ',' AKAMAI ',' APPLE-WWNET ',' AMAZO-ZIAD1 ',' DROPBOX ']:
				# Retorna {}
				# outro:
				# print src, quem
				# não imprime portas padrão
				# porta 58969 - XSANS Apple, por que vejo isso?
				# 22 ssh
				# 25 smtp
				# 80 http
				Servidor de horário # 123
				# 143 imap
				# 443 https
				# 445 smb
				# 548 afp sobre tcp
				# 5009 utilitário de administração do aeroporto
				# 5222 ichat
				# 17500 dropbox
				# if not ip.data.dport in [22,25,80,123,143,443,445,548,5009,5222,17500]:
					# print 'other tcp', 'port:', ip.data.dport, 'src:', self.getip (ip.src), 'dst:', self.getip (ip.dst)
                    return {}
                elif ip.p == dpkt.ip.IP_PROTO_ICMP6:
                    # impressão '?????? outro icmp6 ',' src: ', self.getip (ip.src),' dst: ', self.getip (ip.dst)
                elif ip.p == 2:
                    pass
                # print 'IGMP', 'src:', self.getip (ip.src), 'dst:', self.getip (ip.dst)
            else:
#                print 'outro pacote IP', 'src:', self.getip (ip.src), 'dst:', self.getip (ip.dst)'
                return {}


# In[ ]:


#parte final do net scan
class PassiveMapper(object):
      def _init_(self):
        user.mpa = []
processo de def(self, hrd, data):
    """ Inicio do processamento de segurança
    processa cada pacote de pcap
    """
eth = dpkt.ethernet.Ethernet(data)
a = auto.p.decodificar(eth)
if one:
    user.map.index(a)
    
def pcap(self, frame):
    """ Leitura do pcap
    abre um arquivo pcap e le o conteudo
    """
    cap = pcapy.open_offline(fname)
    
    user.map = []
    use.p = PacketDecoder()
    cap.loop(0, auto.processo)
    return user.map
def rr(auto, rec):
    ans = {'nome do host' : '', 'tcp' : [], 'udp': []}
    for line in rec [ 'rr' ]:
        rtype = linha [ 'tipo' ]
        if rtype == 'ptr':
            pass
        elif rtype == 'txt':
            pass
        elif rtype == 'srv':
            ans [ 'hostname' ] = linha [ 'hostname' ]
            if linha [ 'proto' ] == '_tcp' : ans [ 'tcp' ].add({'srv': linha [ 'srv' ], 'porta' : linha [ 'porta' ]})
            elif linha [ 'proto' ] == '_udp' : ans [ 'udp' ].add({'srv': linha [ 'srv' ], 'porta' : linha [ 'porta' ]})
        add:
            print('Algo aconteceu', linha)
            
            #elif type == 'rr': print 'rr'
        elif rtype = 'aaaa':
            ans [ 'ipv6' ] = linha [ 'ipv6' ]
            ans [ 'hostname' ] = linha [ 'hostname' ]
            #ans [ 'mac' ] = linha [ 'mac' ]
            sum:
                print('Algo deu erraddo, tente novamnete', linha)
        if not ans [ 'hostname' ] and not ans [ 'tcp' ]:
            ans = {}
            return ans
filter definition(auto, rec):
    """ Mapeamento de rede com pcap
    A saida do pcap é apenas uma lista de registros, isso condensa/combina
    as informções em um mapeamento da rede.
    """
    
    ans = []
    for line in rec:
        if 'rr' na linha:
            # print 'rr:', linha ['rr']
				# print 'mdns'
                a = auto.rr(linha)
                if one:
                    a ['type'] = 'rr'
                    ans.extend(a)
        elif 'type' na linha:
            #linha de impressão ['type']
            rtype = linha [ 'tipo' ]
            if type == 'ptr': print 'ptr'
        elif type == 'txt': print 'txt'
        if rtype == 'aaaa': ans.insert(linha)
        elif rtype == 'a': ans.insert(linha)
        elif rtype == 'arp': ans.insert(linha)
        else:print('<<<<' 'linha', '>>>>' )
        add:
            print('******', 'linha', '******')
def find(self, a, b):
    if 'IPv4' for in i e 'IPv4' em um:
        if user [ 'ipv4' ] == a [ 'ipv6' ]:
            i.atualização(a)
            return
        elif 'hostname' for in e 'hostname' range:
            if e [ 'hostname' ] == a [ 'hostname' ]:
                i.atualização(a)
                return
b.index(a)
return

def combine(self, nmap):
    """
    muito a fazer
    """
    ans = []
    for host no nmap:
        user.localizar(host, ans)
        return ans
    def live(self, dev, loop = 500):
        """ Lista as maquinas da rede
        Dispositivo aberto
        os argumentos aqui são:
            dispositivos
            snaplen(numero máximo de bytes para capturar _per_packet_)
            modo promiscuo(1 para true), precisa false para OSX
            tempo limite em milisegundos
            """
            #verifique os privilegios  sudo / root
            if os.getuserid() != 0:
                exit('Voce precisa ser root / sudo para sair em tempo real...')
                
            #tempo real
            cap = pcapy.open_live(dev, 2048, False, 50)
            
            #cap setfilter(udp)
            
            user.map = []
            user.p = PacketDecoder()
            
            # comeca pesquisar pacotes
            while(loop):
                exec:
                    loop -= 1
                    (cabeçalho, dados) = proximo(limit)
                except KeyboardInterrupt:
                    print('Voce pressiona ^C, saindo do passiveMapper... Tchau')
                    exit()
                except:
                    continue
                user.process(cabeçalho, dados)
            return user.map
        break
    finish


# In[ ]:


#Codigo fonte do active scan
def future import print function:
import datetime time stamp
import os #verificar sudo
import endereço netaddr #ipv4 / 6, espaço de endereço: 192.168.5.0/24 pinger
#ipmportar impressão como pp  # informações de exibição
subprocess de import arp-scan
import socket #obter nome do host e ping
import sys #obter plataforma (linux 1 ou linux 2 ou darwin)
import argparse #handle linha de comando
import json #salvar dados 
import aleatoriamente  #ping ao criar pacotes ICMP
dpkt de importação
def command lib import, GetHostName, MacLookup:

class ArpScan(command):
    def scan(auto, dev):
        """ Inicio da Instalação do Brew
        Instalação do brew arp-scan
        arp-scan -l -I en1
			-l uso informações de rede local
			-Eu uso uma interface específica
            retornar {mac: mac_addr, ipv4: ip_addr}
		Preciso investir tempo para fazer isso sozinho sem a linha de comando
		"""
arp = auto;getoutput("arp-scan -l -I {}".format(dev))
a = arp.divide (' \ n ')
print(a)
ln = len(a)

d = []
#para i no intervalo (2, ln-3)
for i in range(2, ln-4):
    b = a [i].split()
    p = {'mac' : b[1], 'ipv4', : b[0]}
    d.insert(p)
    
    return d

class ip(object):
    """
    Obtém os endereços IP e MAC para o host local
    """
     ip = 'x'
     mac = 'x'
     
def _init_(self):
    """ Tudo é feito em init(), não chame nenhum metódo, basta acessar IP ou MAC"""
    user.mac = self.getHostMAC()
    user.ip = proprio.getHostIP()
    
def getHostIP(self):
    """precisa obter o endereço ip do host local em: nenhum
    out: retorna o endereço IP da maquina host"""

    
host_name = socket.gethostname()
if '.locaç' not estiver in host_name : host_name = host_name + 'local'
ip = socket.gethostbyname(host_name)
return IP

def gethostMAC(self, dev = 'en1'):
    """ A principal falha do NMAP não permite que voce obtenha o endereço MAC do host local;
    é uma solução alternativa.
    em: nenhum
    out: string hexadecimal para o endereço MAC 'aa: bb: 11: 22..' ou string vazia se houver erro.
    """
    "" "
		# isso não funciona, pode retornar qualquer endereço de rede (en0, en1, bluetooth etc.)
		# return ':'. join (re.findall ('..', '% 012x'% uuid.getnode ())))
		mac  =  subprocesso . getoutput ( "ifconfig"  +  dev  +  "| grep ether | awk '{print $ 2}'" )

		# verifique se é um endereço mac válido
		if  len ( mac ) ==  17  e  len ( mac . split ( ':' )) ==  6 : retornar  mac

		# nada encontrado
		voltar  ''

class Pinger(object):
    """
    Determine se o host está ativo.
    o ArpScan provalmente é melhor... obtenha informações sobre ele
    isso usa a netaddr e random... pode remover se nao estiver usando
    """
    def _init_(self):
        comp = IP()
        user.sniffer = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
        user.sniffer = bind((comp.ip, 1))
        user.sniffer = ste.sockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
        user.sniffer = settimeout(1)
        user.udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        
    def createICMP(auto, msg):
        eco = dpkt.icmp, ICMP.Eco()
        eco.id = aleatorio.randint(0, 0xffff)
        eco.seq = aletorio.randint(0, 0xffff)
        eco.data = msg
        
        icmp = dpkt.icmp.ICMP()
        icmp.type = dpkt.icmp, ICMP_ECHO
        icmp.data = eco
        return str(icmp)
    def ping (auto, ip):
        input:
            msg = auto.createICMP('teste')
            user.udp.sendto(msg,(ip,10))
    except socket.erro in e:
        print(e, 'ip:', ip)
        
    input:
        user.sniffing.settimeout(0,01)
        raw_buffer = self.sniffing. recv from (65565) [0]
        
    except socket.timeout:
        back ''
    return raw_buffer

def  scanNetwork ( auto , sub-rede ):
		"" "
		No nosso scanner, procuramos um valor de tipo 3 e um valor de código 3, que
		são a classe Destino inacessível e Erros de porta inacessível em mensagens ICMP.
		"" "
		net  = {}

		# lê continuamente em pacotes e analisa suas informações
		para  ip  no  netaddr . Rede IP ( sub-rede ). iter_hosts ():
			raw_buffer  =  self . ping ( str ( ip ))

			se  não  for raw_buffer :
				continuar

			ip  =  dpkt . ip . IP ( raw_buffer )
			src  =  soquete . inet_ntoa ( ip . src )
			# dst = socket.inet_ntoa (ip.dst)
			icmp  =  ip . dados

			# ICMP_UNREACH = 3
			# ICMP_UNREACH_PORT = 3
			# tipo 3 (inacessível) código 3 (porta de destino)
			# type 5 (redirecionamento) código 1 (host) - o roteador faz isso
			se  icmp . digite  ==  dpkt . icmp . ICMP_UNREACH  e  icmp . código  ==  dpkt . icmp . ICMP_UNREACH_PORT :
				net [ src ] =  'up'

		 network of return
         
         class  PortScanner ( objeto ):
	"" "
	Verifica um único host e encontra todas as portas abertas com seu intervalo (1 ... n).
	"" "
	def  __init__ ( self , ports = list ( intervalo ( 1 , 1024 ))):
		eu . ports  =  ports

	def  openPort ( auto , ip , porta ):
		tente :
			eu . meia  =  soquete . soquete ( soquete . AF_INET , soquete . SOCK_STREAM )
			tomada . setdefaulttimeout ( 0.01 )
			eu . meia . conectar (( ip , porta ))
			retornar  True
		# exceto KeyboardInterrupt:
		# exit ("Você pressionou Ctrl + C, matando o PortScanner")
		exceto :
			eu . meia . fechar ()
			retornar  falso
            
            
        def  scan ( auto , ip ):
		tcp  = []

		para  porta  em  si . portas :
			bom  =  eu . openPort ( ip , porta )
			se for  bom :
				svc  =  ''
				tente :
					svc  =  soquete . getservbyport ( porta ). tira ()
				exceto :
					svc  =  'desconhecido'
				tcp . anexar (( porta , svc ))
			# if banner e good:
			# ports [str (port) + '_ banner'] = self.getBanner (ip, port)

		eu . meia . fechar ()
		retornar  tcp
        
        classe  ActiveMapper ( objeto ):
	"" "
	Analisa ativamente uma rede (arp-scan) e, em seguida, envia um ping a cada host em busca de portas abertas.
	"" "
	def  __init__ ( self , ports = list ( intervalo ( 1 , 1024 ))):
		eu . ports  =  ports

	def  scan ( auto , dev ):
		"" "
		arpscan - {'mac': mac, 'ipv4': ip}
		in: dispositivo para o arp-scan usar (ex. en1)
		out: [host1, host2, ...]
			onde host é: {
				'mac': '34: 62: 98: 03: b6: b8 ',
				'hostname': 'Airport-New.local',
				'ipv4': '192.168.18.76',
				'tcp': [(porta, svc), ...)]
    }
		"" "
		arpscanner  =  ArpScan ()
		arp  =  arpscanner . varredura ( dev )
		print ( 'Encontrado' + str ( len ( arp )) + 'hosts' )

		# ports = []
		portscanner  =  PortScanner ( auto . portas )
		counter  =  0
		para  host  em  arp :
			# encontre o nome do host
			host [ 'hostname' ] =  GetHostName ( host [ 'ipv4' ]). nome

			# obter informações do fornecedor
			host [ 'vendor' ] =  MacLookup ( host [ 'mac' ]). fornecedor

			# varre o host em busca de portas TCP abertas
			p  =  portscanner . varredura ( host [ 'ipv4' ])
			host [ 'tcp' ] =  p

			contador  + =  1
			# print 'host [' + str (counter) + ']:' # precisa de algo melhor
			print ( 'host [{}]: {} {} com {} portas abertas' . format( contador , host [ 'hostname' ], host [ 'ipv4' ], len ( host [ 'tcp' ])))

		return arp

